Survey.defaultBootstrapCss.navigationButton = "btn btn-red";
Survey.Survey.cssType = "bootstrap";
Survey.JsonObject.metaData.addProperty("checkbox", {
  name: "renderAs",
  default: "standard",
  choices: ["standard", "nouislider"]
});

var json = {
  locale: "ru",
  pages: [{
      questions: [{
        type: "text",
        isRequired: true,
        name: "В каком городе Вы находитесь?",
        title: "В каком городе Вы находитесь?",
        placeHolder: "например: Москва"
      }]
    },
    {
      questions: [{
        type: "radiogroup",
        isRequired: true,
        renderAs: "nouislider",
        name: "У Вас есть действующий бизнес?",
        title: "У Вас есть действующий бизнес?",
        choices: [{
            value: "У меня стабильный бизнес, более 5 лет ",
            text: "У меня стабильный бизнес, более 5 лет "
          },
          {
            value: "Бизнеса нет, тестирую ниши",
            text: "Бизнеса нет, тестирую ниши "
          },
          {
            value: "Есть дело. Приносит стабильно доход ",
            text: "Есть дело. Приносит стабильно доход "
          },
          {
            value: "Да, есть. Бизнесу от 1 до 3 лет",
            text: "Да, есть. Бизнесу от 1 до 3 лет "
          },
          {
            value: "У меня стартап. Меньше 1 года",
            text: "У меня стартап. Меньше 1 года "
          }
        ]
      }]
    },
    {
      questions: [{
        type: "radiogroup",
        isRequired: true,
        renderAs: "nouislider",
        name: "Вы являетесь собственником этого бизнеса?",
        title: "Вы являетесь собственником этого бизнеса?",
        choices: [{
            value: "Я один из учередителей",
            text: "Я один из учередителей"
          },
          {
            value: "Я являюсь менеджером в компании",
            text: "Я являюсь менеджером в компании"
          },
          {
            value: "Есть дело. Приносит стабильно доход ",
            text: "Есть дело. Приносит стабильно доход "
          },
          {
            value: "Да, я собственник",
            text: "Да, я собственник"
          }
        ]
      }]
    },
    {
      questions: [{
        type: "radiogroup",
        isRequired: true,
        renderAs: "nouislider",
        name: "От куда сейчас привлекаете клиентов?",
        title: "От куда сейчас привлекаете клиентов?",
        choices: [{
            value: "Социальные сети",
            text: "Социальные сети"
          },
          {
            value: "Сарафанное радио",
            text: "Сарафанное радио"
          },
          {
            value: "Есть дело. Приносит стабильно доход ",
            text: "Есть дело. Приносит стабильно доход "
          },
          {
            value: "Сайт",
            text: "Сайт"
          },
          {
            value: "Доски объявлений (Авито/Юла)",
            text: "Доски объявлений (Авито/Юла)"
          }
        ]
      }]
    },
    {
      questions: [{
        type: "radiogroup",
        isRequired: true,
        renderAs: "nouislider",
        name: "У Вашего бизнеса есть странички в соц. сетях?",
        title: "У Вашего бизнеса есть странички в соц. сетях?",
        choices: [{
            value: "Нет, не ведем",
            text: "Нет, не ведем"
          },
          {
            value: "Да, ведем своими силами",
            text: "Да, ведем своими силами"
          },
          {
            value: "Да, ведет специалист. Нам не нравится результат",
            text: "Да, ведет специалист. Нам не нравится результат"
          },
          {
            value: "Да, ведет специалист. Нас все устраивает",
            text: "Да, ведет специалист. Нас все устраивает"
          }
        ]
      }]
    },
    {
      questions: [{
        type: "text",
        name: "Если есть, прикрепите ссылки на Ваши странички. (instagram/vk/facebook)",
        title: "Если есть, прикрепите ссылки на Ваши странички. (instagram/vk/facebook)"
      }]
    },
    {
      questions: [{
        type: "radiogroup",
        isRequired: true,
        renderAs: "nouislider",
        name: "Какой у Вас бизнес?",
        title: "Какой у Вас бизнес?",
        choices: [{
            value: "Производство",
            text: "Производство"
          },
          {
            value: "Торговля, ритейл",
            text: "Торговля, ритейл"
          },
          {
            value: "Услуги",
            text: "Услуги"
          },
          {
            value: "Общепит",
            text: "Общепит"
          }
        ]
      }]
    },
    {
      questions: [{
        type: "checkbox",
        isRequired: true,
        renderAs: "nouislider",
        name: "chek",
        title: "Какой средний чек на Ваш товар/услугу?"
      }]
    },
    {
      questions: [{
        type: "checkbox",
        isRequired: true,
        renderAs: "nouislider",
        name: "budget",
        title: "Ваш примерный бюджет на продвижение в соц. сетях?"
      }]
    },
    {
      questions: [{
          "type": "html",
          "name": "question7",
          "html": "<div class='final-lid'><svg viewBox=\"0 0 24 24\" class=\"mdi-icon mdi-48px\"><title>mdi-check-circle-outline</title><path d=\"M20,12C20,16.42 16.42,20 12,20C7.58,20 4,16.42 4,12C4,7.58 7.58,4 12,4C12.76,4 13.5,4.11 14.2,4.31L15.77,2.74C14.61,2.26 13.34,2 12,2C6.48,2 2,6.48 2,12C2,17.52 6.48,22 12,22C17.52,22 22,17.52 22,12M7.91,10.08L6.5,11.5L11,16L21,6L19.59,4.58L11,13.17L7.91,10.08Z\"></path></svg><h3 class='lid-title'>Отлично, последний шаг!</h3></div>"
        },{

          type: "multipletext",
          isRequired: true,
          name: "Данные клиента",
          title: "Заполните форму, чтобы получить скидку 100% и маркетинговый аудит в подарок",
          items: [
        {
              name: "Имя",
              title: "ВВЕДИТЕ ИМЯ"
            },
            {
              name: "Почта",
              inputType: "email",
              title: "ВВЕДИТЕ EMAIL",
              validators: [{
                type: "email"
              }]
            },
            {
              name: "Телефон",
              inputType: "tel",
              title: "ВВЕДИТЕ ТЕЛЕФОН"
            }
          ]

        },
        {
          "type": "html",
          "name": "question7",
          "html": "<div class='box-white'></div>"
        }
      ]
    }
  ],
  "showQuestionNumbers": "off",
  "showProgressBar": "bottom",
 "pagePrevText": {
  "ru": "⬅"
 },
 "pageNextText": {
  "ru": "Далее ➡"
 },
  "completeText": {
  "ru": "Получить подарок"
 },
  "startSurveyText": {
  "ru": "Получить подарок"
 }
};

var survey = new Survey.Model(json);


var paddingMin = document.getElementById('slider-padding-value-min');
var paddingMax = document.getElementById('slider-padding-value-max');

var widget = {
  name: "nouislider",
  htmlTemplate: "<div id='slider'></div>",
  isFit: function(question) {
    return question["renderAs"] === 'nouislider';
  },
  afterRender: function(question, el) {
    var slider = el.querySelector("#slider");

    noUiSlider.create(slider, {
      start: (question.value && question.value.length) ? question.value : [30, 70],
      connect: true,
      pips: {
        mode: 'steps',
        stepped: true,
        density: 20
      },
      range: {
        'min': 1,
        'max': 1000000
      },
      budget: {
        'min': 1,
        'max': 1000000
      },
      chek: {
        'min': 1,
        'max': 1000000
      }
    });
    slider.noUiSlider.on('update', function (values, handle) {
    if (handle) {
        paddingMax.innerHTML = "до " + values[handle];
    } else {
        paddingMin.innerHTML = "от " + values[handle];
    }
});
  },
  willUnmount: function(question, el) {
    var slider = el.querySelector("#slider");
    slider.noUiSlider.destroy();
  }
}
Survey.CustomWidgetCollection.Instance.addCustomWidget(widget);
survey.data = {
  range: ["300000.00", "600000.00"],
  budget: ["300000.00", "600000.00"],
  chek: ["300000.00", "600000.00"]
};

survey.onComplete.add(function(result) {
  document.querySelector('#result').innerHTML = "result: " + JSON.stringify(result.data);
});

$("#surveyElement").Survey({
  model: survey
});